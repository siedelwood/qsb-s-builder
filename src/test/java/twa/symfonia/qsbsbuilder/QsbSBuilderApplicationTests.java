package twa.symfonia.qsbsbuilder;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import twa.symfonia.qsbsbuilder.model.dependency.LoadOrder;
import twa.symfonia.qsbsbuilder.model.dependency.QsbComponentContainer;
import twa.symfonia.qsbsbuilder.model.update.VersionLog;
import twa.symfonia.qsbsbuilder.service.DownloadManagerService;
import twa.symfonia.qsbsbuilder.service.LoadOrderService;
import twa.symfonia.qsbsbuilder.service.QsbCreatorService;
import twa.symfonia.qsbsbuilder.service.UpdateSourceService;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Enthält die Tests für die Services.
 */
@SpringBootTest
@ActiveProfiles("dev")
class QsbSBuilderApplicationTests {
    @Value("${app.path.test.src}")
    private String pathToSources;

    @Value("${app.path.test.config}")
    private String pathToConfig;

    @Value("${app.path.test.var}")
    private String pathToVar;

    @Value("${drive.id.test.sources}")
    private String driveIdSources;

    @Value("${drive.id.test.version}")
    private String driveIdVersion;

    @Autowired
    private LoadOrderService loadOrderService;

    @Autowired
    private QsbCreatorService qsbCreatorService;

    @Autowired
    private DownloadManagerService downloadManagerService;

    @Autowired
    private UpdateSourceService updateSourceService;

    /**
     * Testet das Laden der Config und des Archives.
     * @throws Exception Fehler
     */
    @Test
    void testDownloadManagerService() throws Exception {
        // Datei laden
        boolean configLoaded = downloadManagerService.downloadFileFromGoogleDrive(
                driveIdVersion,
                pathToVar + "/test-config.json"
        );
        assertTrue(configLoaded);

        // ZIP laden und entpacken
        boolean zipLoaded = downloadManagerService.downloadFileFromGoogleDrive(
                driveIdSources,
                pathToVar + "/test-sources.zip"
        );
        assertTrue(zipLoaded);
        downloadManagerService.unzipFileAndDeleteArchive(pathToVar + "/test-sources.zip");
        File unpackedZip = new File(pathToVar + "/release-sources");
        assertTrue(unpackedZip.exists());
        File docIndex = new File(pathToVar + "/release-sources/sources/doc/index.html");
        assertTrue(docIndex.exists());
        File libCore = new File(pathToVar + "/release-sources/sources/core.lua");
        assertTrue(libCore.exists());
    }

    /**
     * Testet die Aktualisierung aller Dateien.
     * @throws Exception Fehler
     */
    @Test
    void testUpdateSourceService() throws Exception {
        boolean canUpdate = updateSourceService.isUpdateAvailable();
        assertTrue(true);

        List<VersionLog> patchNotes = updateSourceService.getPatchNotes();
        assertTrue(true);

        updateSourceService.updateFiles(true);
        assertTrue(true);
    }

    /**
     * Testet das Laden der Komponenten für die Load Order anhand der Konfiguration.
     * @throws Exception Fehler
     */
    @Test
    void testLoadOrderService() throws Exception {
        QsbComponentContainer componentContainer = loadOrderService.loadFromFile(pathToConfig+ "/bundles.json");
        assertTrue(componentContainer.getComponents().size() > 0 && "Core".equals(componentContainer.getComponents().get(0).getName()));
    }

    /**
     * Testet die Generierung der Load Order aus der Konfiguraationsdatei.
     * @throws Exception Fehler
     */
    @Test
    void testLoadOrder() throws Exception {
        LoadOrder loadOrderModel = loadOrderService.loadFromFileAndCreateLoadOrder(pathToConfig+ "/bundles.json");

        // Teste Dependencies
        List<String> dependencies = loadOrderModel.getDependencies("AddOnQuestDebug");
        assertEquals("Core", dependencies.get(0));
        assertEquals("BundleQuestGeneration", dependencies.get(1));

        // Teste Ancestors
        List<String> ancestors = loadOrderModel.getAncestors("BundleQuestGeneration");
        assertEquals("AddOnQuestStages", ancestors.get(0));
        assertEquals("AddOnQuestDebug", ancestors.get(1));

        // Selektion testen
        loadOrderModel.setSelected("BundleQuestGeneration", true);
    }

    /**
     * Testet die Erzeugung der QSB anhand der Load Order.
     * @throws Exception Fehler
     */
    @Test
    void testQsbCreatorService() throws Exception {
        LoadOrder loadOrderModel = loadOrderService.loadFromFileAndCreateLoadOrder(pathToConfig+ "/bundles.json");
        File qsbFile = qsbCreatorService.createQsb(loadOrderModel, pathToVar, "test-qsb");
        assertNotNull(qsbFile);
    }
}

package twa.symfonia.qsbsbuilder.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import twa.symfonia.qsbsbuilder.model.dependency.LoadOrder;
import twa.symfonia.qsbsbuilder.model.dependency.QsbComponent;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Service zur Erstellung der QSB aus einer Instanz von LoadOrder.
 *
 * Die QSB kann als String zurückgegeben oder als Datei im Dateisystem gespeichert werden.
 */
@Service
public class QsbCreatorService {

    /**
     * Quellverzeichnis QSB
     */
    @Value("${qsb.path.lua}")
    private String pathToSymfoniaLib;

    private String userHome;

    public QsbCreatorService() {
        userHome = System.getProperty("user.home") + File.separator + "Siedelwood" + File.separator + "QSBSBuilder" + File.separator;
    }

    /**
     * Nimmt eine LoadOrder und erzeugt daraus die QSB. Die Datei wird im angegebenen Verzeichnis abgelegt.
     *
     * @param loadOrder    Load Order Instanz
     * @param targetFolder Zielverzeichnis
     * @param fileName     Dateiname (Optional)
     * @return Erzeugte Datei
     * @throws QsbCreatorServiceException Datei nicht erzeugt
     */
    public File createQsb(LoadOrder loadOrder, String targetFolder, String fileName) throws QsbCreatorServiceException {
        String filePath = targetFolder;
        if (null != fileName) {
            filePath = targetFolder+ "/" +fileName;
            if (!filePath.endsWith(".lua")) {
                filePath += ".lua";
            }
        }

        String qsbAsString = createQsbAsString(loadOrder);

        File qsb;
        try {
            // Create file
            Files.write(
                Paths.get(filePath),
                qsbAsString.getBytes(StandardCharsets.UTF_8)
            );

            // Check file
            qsb = new File(filePath);
            if (!qsb.exists()) {
                throw new Exception();
            }
        }
        catch (Exception e) {
            throw new QsbCreatorServiceException("Failed to build qsb file!", e);
        }
        return qsb;
    }

    /**
     * Nimmt eine Load Order und gibt die daraus generierte QSB als String zurück.
     *
     * @param loadOrder Load Order Instanz
     * @return Lua Code als String
     * @throws QsbCreatorServiceException QSB nicht erzeugt
     */
    public String createQsbAsString(LoadOrder loadOrder) throws QsbCreatorServiceException {
        StringBuilder qsbBuilder = new StringBuilder();

        List<String> componentsToLoad = loadOrder.getSelectedComponents();
        for (String name : componentsToLoad) {
            QsbComponent component = loadOrder.getComponentByName(name);
            if ("Core".equals(name)) {
                loadCoreFromFile(qsbBuilder);
            }
            else {
                if (component.isAddon()) {
                    loadAddonFromFile(qsbBuilder, name);
                }
                else if (component.isExternal()) {
                    loadExternalFromFile(qsbBuilder, name);
                }
                else {
                    loadBundleFromFile(qsbBuilder, name);
                }
            }
        }

        return qsbBuilder.toString();
    }

    /**
     * Läd den Core
     * @param sb String Builder
     * @throws QsbCreatorServiceException Code nicht gefunden
     */
    private void loadCoreFromFile(StringBuilder sb) throws QsbCreatorServiceException {
        loadFromFile(sb, userHome + pathToSymfoniaLib+ "/core.lua");
    }

    /**
     * Läd das Bundle mit dem angegeben Namen.
     * @param sb   String Builder
     * @param name Name Bundle
     * @throws QsbCreatorServiceException Code nicht gefunden
     */
    private void loadBundleFromFile(StringBuilder sb, String name) throws QsbCreatorServiceException {
        loadFromFile(sb, userHome + pathToSymfoniaLib+ "/bundles/" +name.toLowerCase()+ ".lua");
    }

    /**
     * Läd das Addon mit dem angegebenen Namen.
     * @param sb   String Builder
     * @param name Name Addon
     * @throws QsbCreatorServiceException Code nicht gefunden
     */
    private void loadAddonFromFile(StringBuilder sb, String name) throws QsbCreatorServiceException {
        loadFromFile(sb, userHome + pathToSymfoniaLib+ "/addons/" +name.toLowerCase()+ ".lua");
    }

    /**
     * Läd das External mit dem angegeben Namen.
     * @param sb   String Builder
     * @param name Name External
     * @throws QsbCreatorServiceException Code nicht gefunden
     */
    private void loadExternalFromFile(StringBuilder sb, String name) throws QsbCreatorServiceException {
        loadFromFile(sb, userHome + pathToSymfoniaLib+ "/external/" +name.toLowerCase()+ ".lua");
    }

    /**
     * Läd die Datei auf dem angegebenen Pfad und fügt ihren Inhalt dem String Builder hinzu.
     *
     * @param sb   String Builder
     * @param path Pfad zur Datei
     * @throws QsbCreatorServiceException Code nicht gefunden
     */
    private void loadFromFile(StringBuilder sb, String path) throws QsbCreatorServiceException {
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(path));
            String content = new String(bytes, StandardCharsets.UTF_8);
            sb.append(content);
        }
        catch (Exception e) {
            throw new QsbCreatorServiceException("Failed to load file!", e);
        }
    }
}

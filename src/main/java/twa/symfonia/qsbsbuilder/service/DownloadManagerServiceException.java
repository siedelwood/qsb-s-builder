package twa.symfonia.qsbsbuilder.service;

public class DownloadManagerServiceException extends Exception {
    public DownloadManagerServiceException() {
    }

    public DownloadManagerServiceException(String s) {
        super(s);
    }

    public DownloadManagerServiceException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DownloadManagerServiceException(Throwable throwable) {
        super(throwable);
    }

    public DownloadManagerServiceException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}

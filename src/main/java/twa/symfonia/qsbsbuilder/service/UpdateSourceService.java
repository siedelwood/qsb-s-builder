package twa.symfonia.qsbsbuilder.service;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsoner;
import org.apache.commons.io.FileUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import twa.symfonia.qsbsbuilder.model.update.Version;
import twa.symfonia.qsbsbuilder.model.update.VersionLog;

import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Service für die Steuerung des Updates von Dateien.
 */
@Service
public class UpdateSourceService {
    @Value("${drive.id.sources}")
    private String driveIdSources;

    @Value("${drive.id.version}")
    private String driveIdVersion;

    @Value("${app.path.var}")
    private String varDirectory;

    @Value("${app.path.config}")
    private String configDirectory;

    @Value("${app.path.src}")
    private String sourcesDirectory;

    private String userHome;

    @Autowired
    private DownloadManagerService downloadManagerService;

    public UpdateSourceService() {
        userHome = System.getProperty("user.home") + File.separator + "Siedelwood" + File.separator + "QSBSBuilder" + File.separator;
    }

    /**
     * Läd die aktuellste Version aus Google Drive herunter, wenn es eine gibt.
     *
     * @param force Update erzwingen
     * @return Update erfolgreich
     * @throws DownloadManagerServiceException Fehler beim Download
     */
    public boolean updateFiles(boolean force) throws DownloadManagerServiceException {
        if (!isUpdateAvailable() && !force) {
            return false;
        }
        try {
            // Update sources
            if (!downloadManagerService.downloadFileFromGoogleDrive(driveIdSources, userHome + varDirectory + "/release-sources.zip")) {
                throw new DownloadManagerServiceException("Unable to download sorces from Google Drive!");
            }
            File sources = new File(userHome + varDirectory + "/release-sources.zip");
            downloadManagerService.unzipFileAndDeleteArchive(sources.getAbsolutePath());
            File oldSources = new File(userHome + sourcesDirectory);
            File newSources = new File(userHome + varDirectory+ "/release-sources/sources");
            if (oldSources.exists()) {
                FileUtils.deleteDirectory(oldSources);
            }
            oldSources.mkdirs();
            FileUtils.copyDirectory(newSources, oldSources);
            // FileUtils.deleteDirectory(newSources);

            // Update version
            File oldConfig = new File(userHome + configDirectory + "/version.json");
            if (oldConfig.exists()) {
                FileUtils.forceDelete(oldConfig);
            }
            Files.copy(Paths.get(userHome + varDirectory + "/release-sources.json"), Paths.get(userHome + configDirectory + "/version.json"));
            // FileUtils.deleteQuietly(new File(userHome + varDirectory + "/release-sources.json"));

            // Aufräumen
            FileUtils.deleteDirectory(new File(userHome + varDirectory));
        }
        catch (DownloadManagerServiceException de) {
            throw de;
        }
        catch (Exception e) {
            throw new DownloadManagerServiceException(e);
        }
        return true;
    }

    /**
     * Gibt alle Changelog Einträge zurück, die in der lokalen Version nicht vorhanden sind.
     *
     * @return Updade verfügbar
     * @throws DownloadManagerServiceException Verbindungs- oder Dateifehler
     */
    public List<VersionLog> getPatchNotes() throws DownloadManagerServiceException {
        Version localLog  = loadLocalVersionLog();
        Version remoteLog = downloadVersionLog();

        List<VersionLog> patchNotes = new ArrayList<>();
        for (int i = remoteLog.getChangelog().size()-1; i>=0; i--) {
            if (!localLog.getChangelog().contains(remoteLog.getChangelog().get(i))) {
                patchNotes.add(remoteLog.getChangelog().get(i));
            }
        }
        return patchNotes;
    }

    /**
     * Prüft, ob ein Update verfügbar ist.
     *
     * @return Updade verfügbar
     */
    public boolean isUpdateAvailable() {
        try {
            Version remoteLog = downloadVersionLog();
            Version localLog = loadLocalVersionLog();
            return localLog.compareTo(remoteLog) < 0;
        }
        catch (DownloadManagerServiceException e) {
            return false;
        }
    }

    /**
     * Läd die Versionshistorie herunter.
     *
     * @return Versionshistorie
     * @throws DownloadManagerServiceException Download fehlgeschlagen
     */
    private Version downloadVersionLog() throws DownloadManagerServiceException {
        Version version = null;
        if (downloadManagerService.downloadFileFromGoogleDrive(driveIdVersion, userHome + varDirectory + "/release-sources.json")) {
            File file = new File(userHome + varDirectory + "/release-sources.json");
            try (FileReader fileReader = new FileReader(file)){
                // Convert
                JsonArray objects = Jsoner.deserializeMany(fileReader);
                Mapper mapper = new DozerBeanMapper();
                JsonObject o = ((JsonArray) objects.get(0)).getMap(0);
                version = mapper.map(o, Version.class);
            }
            catch (Exception e) {
                throw new DownloadManagerServiceException("Unable to aquire version log!", e);
            }
        }
        return version;
    }

    /**
     * Läd die lokale Versionshistorie.
     *
     * @return Versionshistorie
     * @throws DownloadManagerServiceException Datei öffnen fehlgeschlagen
     */
    private Version loadLocalVersionLog() throws DownloadManagerServiceException {
        Version version = null;
            File file = new File(userHome + configDirectory + "/version.json");
            try (FileReader fileReader = new FileReader(file)){
                // Convert
                JsonArray objects = Jsoner.deserializeMany(fileReader);
                Mapper mapper = new DozerBeanMapper();
                JsonObject o = ((JsonArray) objects.get(0)).getMap(0);
                version = mapper.map(o, Version.class);
            }
            catch (Exception e) {
                throw new DownloadManagerServiceException("Unable to aquire version log!", e);
            }
        return version;
    }
}

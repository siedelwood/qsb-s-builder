package twa.symfonia.qsbsbuilder.service;

public class QsbCreatorServiceException extends Exception {
    public QsbCreatorServiceException() {
    }

    public QsbCreatorServiceException(String s) {
        super(s);
    }

    public QsbCreatorServiceException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public QsbCreatorServiceException(Throwable throwable) {
        super(throwable);
    }

    public QsbCreatorServiceException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}

package twa.symfonia.qsbsbuilder.service;

import net.lingala.zip4j.ZipFile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Service für das herunterladen von Dateien.
 */
@Service
public class DownloadManagerService {
    /**
     * Entpackt ein Archiv und löscht es anschließend.
     *
     * @param path Pfad zum Archiv
     * @throws DownloadManagerServiceException Fehler beim entpacken
     */
    public void unzipFileAndDeleteArchive(String path) throws DownloadManagerServiceException {
        File archive = new File(path);
        if (!archive.exists()) {
            throw new DownloadManagerServiceException("Archive was not found!");
        }
        String destDir = archive.getParent();
        if (!unzip(path, destDir)) {
            throw new DownloadManagerServiceException("Archive was not unpacked!");
        }
        if (!archive.delete()) {
            throw new DownloadManagerServiceException("Archive could not be deleted!");
        }
    }

    /**
     * Läd eine Datei von Google Drive herunter.
     *
     * @param id          Drive-ID der Datei
     * @param destination Zielverzeichnis
     * @throws DownloadManagerServiceException
     */
    public boolean downloadFileFromGoogleDrive(String id, String destination) throws DownloadManagerServiceException {
        try {
            return downloadFile(new URL("https://docs.google.com/uc?export=download&id=" +id), destination);
        }
        catch (Exception e) {
            throw new DownloadManagerServiceException("Unable to download from Google Drive!", e);
        }
    }

    /**
     * Läd eine Datei von der angegebenen URL herunter.
     *
     * @param url         URL der Datei
     * @param destination Zielverzeichnis
     * @throws DownloadManagerServiceException
     */
    public boolean downloadFile(URL url, String destination) throws DownloadManagerServiceException {
        try {
            File file = new File(destination);
            file.getParentFile().mkdirs();
            if (!file.exists()) {
                file.createNewFile();
            }
            ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(destination);
            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
            fileOutputStream.flush();
            fileOutputStream.close();
            return file.exists();
        }
        catch (Exception e) {
            throw new DownloadManagerServiceException("Unable to download from url!", e);
        }
    }

    /**
     * Entpackt ein Archiv.
     *
     * @param zipFilePath Pfad zum Archiv
     * @param destDirectory Zielverzeichnis
     * @throws DownloadManagerServiceException Fehler beim entpacken
     */
    public boolean unzip(String zipFilePath, String destDirectory) throws DownloadManagerServiceException {
        try {
            ZipFile zipFile = new ZipFile(new File(zipFilePath));
            zipFile.extractAll(destDirectory);
            return true;
        }
        catch (Exception e) {
            throw new DownloadManagerServiceException(e);
        }
    }

    /**
     * Entpackt ein verschlüsseltes Archiv.
     *
     * @param zipFilePath Pfad zum Archiv
     * @param destDirectory Zielverzeichnis
     * @throws DownloadManagerServiceException Fehler beim entpacken
     */
    public boolean unzip(String zipFilePath, String destDirectory, String password) throws DownloadManagerServiceException {
        try {
            ZipFile zipFile = new ZipFile(new File(zipFilePath));
            zipFile.setPassword(password.toCharArray());
            zipFile.extractAll(destDirectory);
            return true;
        }
        catch (Exception e) {
            throw new DownloadManagerServiceException(e);
        }
    }
}

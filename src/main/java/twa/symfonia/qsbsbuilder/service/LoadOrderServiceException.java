package twa.symfonia.qsbsbuilder.service;

public class LoadOrderServiceException extends Exception {
    public LoadOrderServiceException() {
    }

    public LoadOrderServiceException(String s) {
        super(s);
    }

    public LoadOrderServiceException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public LoadOrderServiceException(Throwable throwable) {
        super(throwable);
    }

    public LoadOrderServiceException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}

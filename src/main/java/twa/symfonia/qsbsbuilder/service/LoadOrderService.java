package twa.symfonia.qsbsbuilder.service;

import com.github.cliftonlabs.json_simple.JsonArray;
import com.github.cliftonlabs.json_simple.Jsoner;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.stereotype.Service;
import twa.symfonia.qsbsbuilder.model.dependency.LoadOrder;
import twa.symfonia.qsbsbuilder.model.dependency.QsbComponent;
import twa.symfonia.qsbsbuilder.model.dependency.QsbComponentContainer;
import twa.symfonia.qsbsbuilder.model.dependency.QsbComponentType;

import java.io.FileReader;
import java.util.List;

/**
 * Service zum laden der Load Order.
 */
@Service
public class LoadOrderService {
    /**
     * Gibt eine Load Order Instanz zurück basierend auf der Konfigurationsdatei.
     *
     * @param file Pfad zur Datei
     * @return Load Order
     * @throws LoadOrderServiceException Datei nicht geladen
     */
    public LoadOrder loadFromFileAndCreateLoadOrder(final String file) throws LoadOrderServiceException {
        QsbComponentContainer componentContainer = loadFromFile(file);
        List<QsbComponent> componentList = componentContainer.getComponents();
        LoadOrder loadOrder = new LoadOrder();
        loadOrder.init(componentList, componentContainer.getVersion());
        return loadOrder;
    }

    /**
     * Läd die Load Order aus der angegebenen Datei.
     *
     * @param file Pfad zur Datei
     * @return Liste von Komponenten
     * @throws LoadOrderServiceException Datei nicht geladen
     */
    public QsbComponentContainer loadFromFile(final String file) throws LoadOrderServiceException {
        QsbComponentContainer componentContainer;
        try (FileReader fileReader = new FileReader(file)) {
            // Convert
            JsonArray objects = Jsoner.deserializeMany(fileReader);
            Mapper mapper = new DozerBeanMapper();
            JsonArray o = (JsonArray) objects.get(0);
            componentContainer = mapper.map(o.get(0), QsbComponentContainer.class);
            // Set type
            componentContainer.getComponents().forEach(c -> {
                if (!c.isAddon() && !c.isExternal())
                    c.setType(QsbComponentType.BUNDLE);
                if (c.isAddon() && !c.isExternal())
                    c.setType(QsbComponentType.ADDON);
                if (!c.isAddon() && c.isExternal())
                    c.setType(QsbComponentType.EXTERNAL);
            });
        } catch (Exception e) {
            throw new LoadOrderServiceException("Failed to initalize the load order!", e);
        }
        return componentContainer;
    };
}

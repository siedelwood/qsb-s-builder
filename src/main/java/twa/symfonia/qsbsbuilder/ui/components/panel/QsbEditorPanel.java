package twa.symfonia.qsbsbuilder.ui.components.panel;

import lombok.Getter;
import twa.symfonia.qsbsbuilder.model.dependency.LoadOrder;
import twa.symfonia.qsbsbuilder.model.dependency.QsbComponent;
import twa.symfonia.qsbsbuilder.ui.components.frame.MainWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Dieses Panel stellt die Auswahlseite für die Optionen dar.
 *
 * Alle anderen Panel werden ausgeblendet und dieses an ihrer Stelle angezeigt.
 */
public class QsbEditorPanel extends JPanel implements ActionListener {
    private MainWindow parentWindow;
    private int panelHeight;
    private int panelWidth;

    @Getter
    private JTextField folderField;
    @Getter
    private JPanel bundleSelectionPanel;
    @Getter
    private LoadOrder loadOrderModel;
    @Getter
    private List<JCheckBox> componentCheckboxList;

    /**
     * @param window Fenster
     * @param width  Breite
     * @param height Höhe
     */
    public QsbEditorPanel(MainWindow window, int width, int height) {
        super();
        parentWindow = window;
        panelHeight = height;
        panelWidth = width;
        componentCheckboxList = new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    public void initPanel() {
        setLayout(null);
        setBounds(0, 0, panelWidth, panelHeight);

        createUpdateGoup();
        createControlGoup();
        setVisible(false);
    }

    /**
     * Zeigt Zustimmen Button und Ablehnen Button an.
     */
    private void createControlGoup() {
        JButton confirm = new JButton("Speichern >");
        confirm.setName("EditorPage_ConfirmSaving");
        confirm.addActionListener(parentWindow.getApplicationInterfaceController());
        confirm.setBounds(panelWidth -145, panelHeight -80, 120, 25);
        add(confirm);

        JButton decline = new JButton("< Abbrechen");
        decline.setName("EditorPage_DeclineSaving");
        decline.addActionListener(parentWindow.getApplicationInterfaceController());
        decline.setBounds(10, panelHeight -80, 120, 25);
        add(decline);
    }

    /**
     * Zeigt den Viewer für die Änderungen an-
     */
    private void createUpdateGoup() {
        JPanel editorGroup = new JPanel(null);
        editorGroup.setBounds(5, 5, panelWidth -25, panelHeight -90);
        editorGroup.setBorder(BorderFactory.createTitledBorder("Editor"));
        add(editorGroup);

        String text = ""+
            "In diesem Fenster wird die QSb aus den Quellen von Symfonia zusammengestellt. Wähle die Module aus," +
            " welche enthalten seien sollen. Abhängigkeiten werden automatisch beachtet.";

        JLabel description = new JLabel("<html>" +text+ "</html>");
        description.setFont(new Font("Arial", Font.PLAIN, 12));
        description.setBounds(10, 20, editorGroup.getWidth() -20, 30);
        description.setVisible(true);
        editorGroup.add(description);


        int selectionWidth = panelWidth -45;
        int selectionHeight = panelHeight -250;
        JPanel bundleSelectionGroup = new JPanel(null);
        bundleSelectionGroup.setBounds(10, 55, selectionWidth, selectionHeight);
        bundleSelectionGroup.setBorder(BorderFactory.createTitledBorder("Verfügbare Komponenten"));
        editorGroup.add(bundleSelectionGroup);

        JButton selectAll = new JButton("Alle auswählen");
        selectAll.setName("EditorPage_SelectAll");
        selectAll.setBounds(10, panelHeight - 280, 120, 20);
        selectAll.addActionListener(this);
        bundleSelectionGroup.add(selectAll);

        JButton resetSelection = new JButton("Zurücksetzen");
        resetSelection.setName("EditorPage_ResetSelection");
        resetSelection.setBounds(140, panelHeight - 280, 120, 20);
        resetSelection.addActionListener(this);
        bundleSelectionGroup.add(resetSelection);

        JButton openDoc = new JButton("Dokumentation");
        openDoc.setName("EditorPage_OpenDocumentation");
        openDoc.setBounds(selectionWidth -130, panelHeight - 280, 120, 20);
        openDoc.addActionListener(parentWindow.getApplicationInterfaceController());
        bundleSelectionGroup.add(openDoc);

        bundleSelectionPanel = new JPanel(null);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setViewportView(bundleSelectionPanel);
        scrollPane.setVisible(true);
        scrollPane.setBounds(7, 20, selectionWidth -15, selectionHeight -55);
        bundleSelectionGroup.add(scrollPane);

        addComponentCheckboxes(selectionWidth -15, selectionHeight -55);
        updateComponentCheckboxes();


        JCheckBox globalScript = new JCheckBox("Globales Skript kopieren");
        globalScript.setName("EditorPage_GlobalScript");
        globalScript.setBounds(10, panelHeight - 190, 500, 15);
        globalScript.addActionListener(parentWindow.getApplicationInterfaceController());
        globalScript.setSelected(true);
        editorGroup.add(globalScript);

        JCheckBox localScript = new JCheckBox("Lokales Skript kopieren");
        localScript.setName("EditorPage_LocalScript");
        localScript.setBounds(10, panelHeight - 170, 500, 15);
        localScript.addActionListener(parentWindow.getApplicationInterfaceController());
        localScript.setSelected(true);
        editorGroup.add(localScript);

        String downloadDirectory = System.getProperty("user.home") + File.separator + "Downloads";
        folderField = new JTextField(downloadDirectory);
        folderField.setName("EditorPage_Destination");
        folderField.setBounds(140, panelHeight - 135, panelWidth -175, 20);
        folderField.setEditable(false);
        editorGroup.add(folderField);

        JButton selectDirectory = new JButton("Suchen");
        selectDirectory.setName("EditorPage_SelectDestination");
        selectDirectory.setBounds(10, panelHeight - 137, 120, 25);
        selectDirectory.addActionListener(this);
        editorGroup.add(selectDirectory);

    }

    /**
     * Generiert die Checkboxen der Komponenten anhand der Load Order.
     *
     * @param selectionWidth  Breite des Panel
     * @param selectionHeight Höhe des Panel
     */
    private void addComponentCheckboxes(int selectionWidth, int selectionHeight) {
        // Load Order erstellen
        if (null == loadOrderModel) {
            loadOrderModel = parentWindow.getApplicationFeatureController().getLoadOrder();
        }
        // Abbruch bei Fehlschlag
        if (null == loadOrderModel) {
            return;
        }

        int newHeight = 5;
        List<QsbComponent> componentList = loadOrderModel.getComponentList();
        for (QsbComponent c : componentList) {
            JCheckBox checkbox = new JCheckBox(c.getDisplayName());
            checkbox.setName(c.getName());
            checkbox.setBounds(10, newHeight, selectionWidth -30, 20);
            checkbox.addActionListener(this);
            checkbox.setToolTipText(c.getDescription());
            if (c.isMandatory()) {
                checkbox.setEnabled(false);
            }

            newHeight = newHeight +20;
            bundleSelectionPanel.setSize(selectionWidth, newHeight);
            bundleSelectionPanel.add(checkbox);

            componentCheckboxList.add(checkbox);
        }
        newHeight = newHeight + 10;
        bundleSelectionPanel.setSize(selectionWidth, newHeight);
        bundleSelectionPanel.setPreferredSize(new Dimension(selectionWidth, newHeight));
    }

    /**
     * Aktualisiert die Checkbox Liste und setzt die ausgewählten Komponenten.
     */
    public void updateComponentCheckboxes() {
        // Load Order erstellen
        if (null == loadOrderModel) {
            loadOrderModel = parentWindow.getApplicationFeatureController().getLoadOrder();
        }
        // Abbruch bei Fehlschlag
        if (null == loadOrderModel) {
            return;
        }

        for (JCheckBox cb : componentCheckboxList) {
            cb.setSelected(false);
        }

        List<String> selectedComponents = loadOrderModel.getSelectedComponents();
        for (String s : selectedComponents) {
            for (JCheckBox cb : componentCheckboxList) {
                if (cb.getName().equals(s)) {
                    cb.setSelected(true);
                }
            }
        }
    }

    // Action Listener //

    @Override
    public void actionPerformed(ActionEvent e) {
        JComponent source = (JComponent) e.getSource();
        for (JCheckBox cb : componentCheckboxList) {
            if (source == cb) {
                String componentName = cb.getName();
                parentWindow.getApplicationInterfaceController().selectQsbComponent(componentName, cb.isSelected(), loadOrderModel);
            }
        }

        if ("EditorPage_SelectAll".equals(source.getName())) {
            parentWindow.getApplicationInterfaceController().selectAllQsbComponents(loadOrderModel);
        }
        if ("EditorPage_ResetSelection".equals(source.getName())) {
            parentWindow.getApplicationInterfaceController().resetQsbComponents(loadOrderModel);
        }
        if ("EditorPage_SelectDestination".equals(source.getName())) {
            parentWindow.getApplicationInterfaceController().openDestinationFileChooser();
            folderField.setText(parentWindow.getApplicationInterfaceController().getLastDirectory().getAbsolutePath());
        }
    }
}

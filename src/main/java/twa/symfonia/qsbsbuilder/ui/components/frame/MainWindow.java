package twa.symfonia.qsbsbuilder.ui.components.frame;

import lombok.Getter;
import lombok.Setter;
import twa.symfonia.qsbsbuilder.ui.components.panel.ConvertMapPanel;
import twa.symfonia.qsbsbuilder.ui.components.panel.OngoingProcessPanel;
import twa.symfonia.qsbsbuilder.ui.components.panel.QsbEditorPanel;
import twa.symfonia.qsbsbuilder.ui.components.panel.UpdateSourcesPanel;
import twa.symfonia.qsbsbuilder.ui.components.panel.UserInterfacePanel;

import javax.swing.*;

/**
 * Fenster der Applikation.
 */
public class MainWindow extends AbstractWindow {
    private final int windowHeight = 600;
    private final int windowWidth = 800;
    private final String windowTitle = "QSB";

    private JPanel mainPanel;

    @Getter
    private OngoingProcessPanel ongoingProcessPanel;
    @Getter
    private UserInterfacePanel userInterfacePanel;
    @Getter
    private UpdateSourcesPanel updateSourcesPanel;
    @Getter
    private QsbEditorPanel qsbEditorPanel;
    @Getter
    private ConvertMapPanel convertMapPanel;

    @Setter
    @Getter
    private String version;

    public MainWindow() {
        super();
    }

    @Override
    public void initComponents() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(0, 0, windowWidth, windowHeight);
        setTitle(windowTitle);
        setLocationRelativeTo(null);
        setResizable(false);

        mainPanel = new JPanel(null);
        mainPanel.setBounds(0, 0, windowWidth, windowHeight);
        add(mainPanel);

        ongoingProcessPanel = new OngoingProcessPanel(this, windowWidth, windowHeight);
        mainPanel.add(ongoingProcessPanel);
        ongoingProcessPanel.initPanel();

        userInterfacePanel = new UserInterfacePanel(this, windowWidth, windowHeight);
        mainPanel.add(userInterfacePanel);
        userInterfacePanel.initPanel();

        updateSourcesPanel = new UpdateSourcesPanel(this, windowWidth, windowHeight);
        mainPanel.add(updateSourcesPanel);
        updateSourcesPanel.initPanel();

        qsbEditorPanel = new QsbEditorPanel(this, windowWidth, windowHeight);
        mainPanel.add(qsbEditorPanel);
        qsbEditorPanel.initPanel();

        convertMapPanel = new ConvertMapPanel(this, windowWidth, windowHeight);
        mainPanel.add(convertMapPanel);
        convertMapPanel.initPanel();
        convertMapPanel.setVisible(false);

        setVisible(true);
        setOngoingProcessPanelVisibility(false);
        setUserInterfacePanelVisibility(true);
        setUpdateSourcesPanelVisibility(false);
        setMapConversationPanelVisibility(false);
    }

    public void setOngoingProcessPanelVisibility(boolean flag) {
        ongoingProcessPanel.setVisible(flag);
    }

    public void setUserInterfacePanelVisibility(boolean flag) {
        userInterfacePanel.setVisible(flag);
    }

    public void setUpdateSourcesPanelVisibility(boolean flag) {
        updateSourcesPanel.setVisible(flag);
    }

    public void setQsbEditorPanelVisibility(boolean flag) {
        qsbEditorPanel.setVisible(flag);
    }

    public void setMapConversationPanelVisibility(boolean flag) {
        convertMapPanel.setVisible(flag);
    }
}

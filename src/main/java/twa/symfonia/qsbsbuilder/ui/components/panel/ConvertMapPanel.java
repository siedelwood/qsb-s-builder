package twa.symfonia.qsbsbuilder.ui.components.panel;

import lombok.Getter;
import twa.symfonia.qsbsbuilder.ui.components.frame.MainWindow;

import javax.swing.*;

/**
 * Dieses Panel wird für die Konvertierung einer Map von QSB 3.9 zu QSB-S verwendet.
 *
 * Alle anderen Panel werden ausgeblendet und dieses an ihrer Stelle angezeigt.
 */
public class ConvertMapPanel extends JPanel {
    private MainWindow parentWindow;
    private int panelHeight;
    private int panelWidth;

    @Getter
    private JTextArea changeLog;

    /**
     * @param window Fenster
     * @param width  Breite
     * @param height Höhe
     */
    public ConvertMapPanel(MainWindow window, int width, int height) {
        super();
        parentWindow = window;
        panelHeight = height;
        panelWidth = width;
    }

    /**
     * {@inheritDoc}
     */
    public void initPanel() {
        setLayout(null);
        setBounds(0, 0, panelWidth, panelHeight);

        createControlGoup();
    }

    /**
     * Zeigt Zustimmen Button und Ablehnen Button an.
     */
    private void createControlGoup() {
        JButton confirm = new JButton("Umwandeln >");
        confirm.setName("ConvertPage_ConfirmConvert");
        confirm.addActionListener(parentWindow.getApplicationInterfaceController());
        confirm.setBounds(panelWidth -145, panelHeight -80, 120, 25);
        add(confirm);

        JButton decline = new JButton("< Abbrechen");
        decline.setName("ConvertPage_DeclineConvert");
        decline.addActionListener(parentWindow.getApplicationInterfaceController());
        decline.setBounds(10, panelHeight -80, 120, 25);
        add(decline);
    }
}

package twa.symfonia.qsbsbuilder.ui.components.panel;

import twa.symfonia.qsbsbuilder.ui.components.frame.MainWindow;

import javax.swing.*;
import java.awt.*;

/**
 * Dieses Panel wird als allgemeine Anzeige für einen laufenden Prozess genutzt.
 *
 * Alle anderen Panel werden ausgeblendet und dieses an ihrer Stelle angezeigt.
 */
public class UserInterfacePanel extends JPanel {
    private MainWindow parentWindow;
    private int panelHeight;
    private int panelWidth;

    /**
     * @param window Fenster
     * @param width  Breite
     * @param height Höhe
     */
    public UserInterfacePanel(MainWindow window, int width, int height) {
        super();
        parentWindow = window;
        panelHeight = height;
        panelWidth = width;
    }

    /**
     * {@inheritDoc}
     */
    public void initPanel() {
        setLayout(null);
        setBounds(0, 0, panelWidth, panelHeight);

        createUpdateGoup();
        createQsbGroup();
        // Keine Ahnung, ob das noch sinnvoll wäre.
        // createConvertGroup();
        createVersion();
    }

    /**
     * Version des Programms unten anzeigen.
     */
    private void createVersion() {
        JPanel panel = new JPanel(null);
        panel.setBounds(10, panelHeight -70, panelWidth -25, 15);
        add(panel);

        JLabel textLabel = new JLabel("<html>Version: " +parentWindow.getVersion()+ "</html>");
        textLabel.setBounds(0, 0, panel.getWidth()-20, 15);
        textLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        panel.add(textLabel);
    }

    /**
     * Anzeige der Gruppe "QSB zusammen stellen".
     */
    private void createQsbGroup() {
        JPanel panel = new JPanel(null);
        panel.setBounds(5, (int) ((panelHeight * 0.12) +15), panelWidth -25, (int) (panelHeight * 0.12));
        panel.setBorder(BorderFactory.createTitledBorder("QSB zusammen stellen"));
        add(panel);

        String text = "Du kannst dir aus den Quelldateien von Symfonia deine eigene persönliche QSB zusammen stellen.";
        JLabel textLabel = new JLabel("<html>" +text+ "</html>");
        textLabel.setBounds(10, 20, panel.getWidth()-20, 15);
        textLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        panel.add(textLabel);

        JButton update = new JButton("Editor öffnen");
        update.setName("SelectionPage_ComposeButton");
        update.setBounds(10, panel.getHeight() -35, 120, 25);
        update.addActionListener(parentWindow.getApplicationInterfaceController());
        panel.add(update);
    }

    /**
     * Anzeige der Gruppe "Nach Updates suchen".
     */
    private void createUpdateGoup() {
        JPanel panel = new JPanel(null);
        panel.setBounds(5, 5, panelWidth -25, (int) (panelHeight * 0.12));
        panel.setBorder(BorderFactory.createTitledBorder("Nach Update suchen"));
        add(panel);

        JButton update = new JButton("Aktualisieren");
        update.setName("SelectionPage_UpdateButton");
        update.setBounds(10, panel.getHeight() -35, 120, 25);
        update.addActionListener(parentWindow.getApplicationInterfaceController());
        panel.add(update);

        String text = "Die Anwendung sucht nach Updates. Wenn ein Update verfügbar ist, kannst du es herunterladen.";
        JLabel textLabel = new JLabel("<html>" +text+ "</html>");
        textLabel.setBounds(10, 20, panel.getWidth()-20, 15);
        textLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        panel.add(textLabel);
    }

    /**
     * Anzeige der Gruppe "Map umwandeln".
     */
    private void createConvertGroup() {
        JPanel panel = new JPanel(null);
        panel.setBounds(5, (int) ((panelHeight * 0.24) +25), panelWidth -25, (int) (panelHeight * 0.12));
        panel.setBorder(BorderFactory.createTitledBorder("Map umwandeln"));
        add(panel);

        JButton update = new JButton("Konvertieren");
        update.setName("SelectionPage_ConvertButton");
        update.setBounds(10, panel.getHeight() -35, 120, 25);
        update.addActionListener(parentWindow.getApplicationInterfaceController());
        panel.add(update);

        String text = "Du kannst eine Map, welche mit der QSB 3.9 erstellt wurde für die QSB-S umwandeln.";
        JLabel textLabel = new JLabel("<html>" +text+ "</html>");
        textLabel.setBounds(10, 20, panel.getWidth()-20, 15);
        textLabel.setFont(new Font("Arial", Font.PLAIN, 12));
        panel.add(textLabel);
    }
}

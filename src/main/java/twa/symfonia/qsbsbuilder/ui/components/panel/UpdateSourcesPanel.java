package twa.symfonia.qsbsbuilder.ui.components.panel;

import lombok.Getter;
import twa.symfonia.qsbsbuilder.ui.components.frame.MainWindow;

import javax.swing.*;
import java.awt.*;

/**
 * Dieses Panel wird für die Aktualisierung der Quelldateien genutzt.
 *
 * Alle anderen Panel werden ausgeblendet und dieses an ihrer Stelle angezeigt.
 */
public class UpdateSourcesPanel extends JPanel {
    private MainWindow parentWindow;
    private int panelHeight;
    private int panelWidth;

    @Getter
    private JTextArea changeLog;

    /**
     * @param window Fenster
     * @param width  Breite
     * @param height Höhe
     */
    public UpdateSourcesPanel(MainWindow window, int width, int height) {
        super();
        parentWindow = window;
        panelHeight = height;
        panelWidth = width;
    }

    /**
     * {@inheritDoc}
     */
    public void initPanel() {
        setLayout(null);
        setBounds(0, 0, panelWidth, panelHeight);

        createUpdateGoup();
        createControlGoup();
    }

    /**
     * Zeigt Zustimmen Button und Ablehnen Button an.
     */
    private void createControlGoup() {
        JButton confirm = new JButton("Aktualisieren >");
        confirm.setName("UpdatePage_ConfirmUpdate");
        confirm.addActionListener(parentWindow.getApplicationInterfaceController());
        confirm.setBounds(panelWidth -145, panelHeight -80, 120, 25);
        add(confirm);

        JButton decline = new JButton("< Abbrechen");
        decline.setName("UpdatePage_CancleUpdate");
        decline.addActionListener(parentWindow.getApplicationInterfaceController());
        decline.setBounds(10, panelHeight -80, 120, 25);
        add(decline);
    }

    /**
     * Zeigt den Viewer für die Änderungen an-
     */
    private void createUpdateGoup() {
        JPanel updatePreviewGroup = new JPanel(null);
        updatePreviewGroup.setBounds(5, 5, panelWidth -25, panelHeight -90);
        updatePreviewGroup.setBorder(BorderFactory.createTitledBorder("Update"));
        add(updatePreviewGroup);

        String debugText = ""+
            "Version: 2.7.3\nÄnderungen:\n- Bugfix: Timing-Problem beim direkten Bezahlen von interaktiven" +
            " Objekten gefixt.\n\n------------------------------\n\nVersion: 2.7.2\nÄnderungen:\n- Bugfix: " +
            "Automatische Behavior werden nun hinzugefügt.\n\n------------------------------\n\nVersion: " +
            "2.7.1\nÄnderungen:\n- Bugfix: Funktionsreferenzen in *_MapScriptFunction korrigiert.\n- " +
            "Bugfix: :CountSoldiers() gibt nun korrekte Werte zurück.\n";

        changeLog = new JTextArea();
        changeLog.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        changeLog.setDisabledTextColor(new Color(40, 40, 40));
        changeLog.setWrapStyleWord(true);
        changeLog.setEnabled(false);
        changeLog.setVisible(true);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setViewportView(changeLog);
        scrollPane.setVisible(true);
        scrollPane.setBounds(10, 60, updatePreviewGroup.getWidth() -20, updatePreviewGroup.getHeight() -70);
        changeLog.setSize(updatePreviewGroup.getWidth() -20, updatePreviewGroup.getHeight() -70);
        updatePreviewGroup.add(scrollPane);

        String text = ""+
            "Ein neues Update ist bereit eingespielt zu werden.<br>Prüfe die Änderungen zu der lokalen Version der"+
            " QSB und führe dann das Update aus. Ein anschileßender Neustart ist <b>nicht</b> notwendig.";

        JLabel description = new JLabel("<html>" +text+ "</html>");
        description.setFont(new Font("Arial", Font.PLAIN, 12));
        description.setBounds(10, 20, updatePreviewGroup.getWidth() -20, 30);
        description.setVisible(true);
        updatePreviewGroup.add(description);
    }
}

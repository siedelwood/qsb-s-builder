package twa.symfonia.qsbsbuilder.ui.components.panel;

import twa.symfonia.qsbsbuilder.ui.components.frame.MainWindow;

import javax.swing.*;

/**
 * Dieses Panel wird als allgemeine Anzeige für einen laufenden Prozess genutzt.
 *
 * Alle anderen Panel werden ausgeblendet und dieses an ihrer Stelle angezeigt.
 */
public class OngoingProcessPanel extends JPanel {
    private MainWindow parentWindow;
    private int panelHeight;
    private int panelWidth;

    /**
     * @param window Fenster
     * @param width  Breite
     * @param height Höhe
     */
    public OngoingProcessPanel(MainWindow window, int width, int height) {
        super();
        parentWindow = window;
        panelHeight = height;
        panelWidth = width;
    }

    /**
     * {@inheritDoc}
     */
    public void initPanel() {
        setLayout(null);
        setBounds(0, 0, panelWidth, panelHeight);

        JProgressBar progressBar = new JProgressBar();
        add(progressBar);
        progressBar.setIndeterminate(true);
        progressBar.setBounds(190, (int) ((panelHeight * 0.42) + 0), panelWidth - 400, 20);

        JLabel text = new JLabel("<html><b>Bitte warten...</b></html>");
        add(text);
        text.setBounds(190, (int) ((panelHeight * 0.42) -15), panelWidth -400, 15);
    }
}

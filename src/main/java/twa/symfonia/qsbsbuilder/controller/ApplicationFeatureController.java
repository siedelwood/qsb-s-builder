package twa.symfonia.qsbsbuilder.controller;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import twa.symfonia.qsbsbuilder.model.dependency.LoadOrder;
import twa.symfonia.qsbsbuilder.model.update.VersionLog;
import twa.symfonia.qsbsbuilder.service.DownloadManagerServiceException;
import twa.symfonia.qsbsbuilder.service.LoadOrderService;
import twa.symfonia.qsbsbuilder.service.LoadOrderServiceException;
import twa.symfonia.qsbsbuilder.service.QsbCreatorService;
import twa.symfonia.qsbsbuilder.service.UpdateSourceService;

import java.io.File;
import java.util.List;

/**
 * Controller zur Steuerung der Prozesse der Anwendung.
 */
@Component
public class ApplicationFeatureController {
    private static Logger LOG = LoggerFactory.getLogger(CommandLineRunner.class);

    @Setter
    @Getter
    private Exception lastException;

    private String userHome;

    @Value("${app.path.config}")
    private String pathToConfig;

    @Value("${app.path.var}")
    private String pathToVar;

    @Value("${app.path.src}")
    private String pathToSources;

    @Value("${drive.id.sources}")
    private String driveIdSources;

    @Value("${drive.id.version}")
    private String driveIdVersion;

    @Autowired
    private UpdateSourceService updateSourceService;

    @Autowired
    private LoadOrderService loadOrderService;

    @Autowired
    private QsbCreatorService qsbCreatorService;

    public ApplicationFeatureController() {
        userHome = System.getProperty("user.home") + File.separator + "Siedelwood" + File.separator + "QSBSBuilder" + File.separator;
    }

    /**
     * Erzwingt das Update aller downloadbaren Dateien.
     *
     * @return Update erfolgreich
     */
    public boolean forceUpdateFiles() {
        try {
            return updateSourceService.updateFiles(true);
        }
        catch (DownloadManagerServiceException e) {
            LOG.error("Forced update failed!", e);
            lastException = e;
        }
        return false;
    }

    /**
     * Führt ein Update aller herunterladbaren Dateien aus, falls notwendig.
     *
     * @return Update erfolgreich
     */
    public boolean updateFiles() {
        try {
            return updateSourceService.updateFiles(false);
        }
        catch (Exception e) {
            LOG.error("Regular update failed!", e);
            lastException = e;
        }
        return false;
    }

    /**
     * Prüft, ob ein Update notwendig ist.
     *
     * @return Update notwendig
     */
    public boolean areUpdatesAvailable() {
        return updateSourceService.isUpdateAvailable();
    }

    /**
     * Gibt neue Änderungen als String zurück.
     *
     * @return String
     */
    public String getPatchNotes() {
        String text = "";
        try{
            List<VersionLog> patchNotes = updateSourceService.getPatchNotes();
            if (patchNotes.size() > 0) {
                StringBuilder sb = new StringBuilder();
                for (int i=patchNotes.size()-1; i>=0; i--) {
                    // Version
                    String version = patchNotes.get(i).getVersion();
                    sb.append("Version: ").append(version).append("\n");

                    // Änderungen
                    List<String> changes = patchNotes.get(i).getChanges();
                    sb.append("Änderungen:").append("\n");
                    for (int j=0; j<changes.size(); j++) {
                        sb.append("- ").append(changes.get(j)).append("\n");
                    }

                    if (i > 0) {
                        sb.append("\n------------------------------------------------------------\n\n");
                    }
                }
                text = sb.toString();
            }
        }
        catch (Exception e) {
            LOG.error("Could not load patch notes!", e);
            lastException = e;
        }
        return text;
    }

    /**
     * Baut die QSB zusammen und gibt die erzeugte Datei zurück.
     *
     * @param path Pfad der Ausgabedatei
     * @return QSB Datei
     */
    public File saveQsb(String path, LoadOrder loadOrder) {
        try {
            File qsbFile;
            LoadOrder loadOrderModel = loadOrder;
            if (null == loadOrderModel) {
                loadOrderModel = loadOrderService.loadFromFileAndCreateLoadOrder(userHome + pathToConfig + "/bundles.json");
            }
            if (null != path) {
                if (!path.endsWith(".lua")) {
                    path += ".lua";
                }
                qsbFile = qsbCreatorService.createQsb(loadOrderModel, path, null);
            }
            else {
                qsbFile = qsbCreatorService.createQsb(loadOrderModel, path, "qsb");
            }
            return qsbFile;
        }
        catch (Exception e) {
            LOG.error("Qsb was not created!", e);
            lastException = e;
        }
        return null;
    }

    /**
     * Erzeugt die Load Order aus der Konfiguration.
     *
     * @return Load Order
     */
    public LoadOrder getLoadOrder() {
        try {
            return loadOrderService.loadFromFileAndCreateLoadOrder(userHome + pathToConfig + "/bundles.json");
        }
        catch (LoadOrderServiceException e) {
            LOG.error("Qsb was not created!", e);
            lastException = e;
        }
        return null;
    }
}

package twa.symfonia.qsbsbuilder.controller;

import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import twa.symfonia.qsbsbuilder.model.dependency.LoadOrder;
import twa.symfonia.qsbsbuilder.model.dependency.QsbComponent;
import twa.symfonia.qsbsbuilder.service.DownloadManagerService;
import twa.symfonia.qsbsbuilder.service.LoadOrderService;
import twa.symfonia.qsbsbuilder.service.LoadOrderServiceException;
import twa.symfonia.qsbsbuilder.ui.SwingMessageService;
import twa.symfonia.qsbsbuilder.ui.components.frame.MainWindow;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Controller zur Reaktion auf die Aktionen des Benutzers.
 */
@Component
public class ApplicationInterfaceController extends AbstractInterfaceController {
    private static Logger LOG = LoggerFactory.getLogger(ApplicationInterfaceController.class);

    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private ApplicationFeatureController applicationFeatureController;
    @Autowired
    private SwingMessageService swingMessageService;
    @Autowired
    private LoadOrderService loadOrderService;
    @Autowired
    private DownloadManagerService downloadManagerService;

    @Getter
    private Exception lastException;
    @Getter
    private File lastDirectory;
    @Getter
    private MainWindow windowFrame;

    private String userHome;

    @Value("${app.version}")
    private String appVersion;
    @Value("${app.path.config}")
    private String pathToConfig;
    @Value("${app.path.var}")
    private String pathToVar;
    @Value("${app.path.src}")
    private String pathToSources;
    @Value("${drive.id.sources}")
    private String driveIdSources;
    @Value("${drive.id.version}")
    private String driveIdVersion;
    @Value("${drive.id.bundles}")
    private String driveIdBundles;

    private boolean copyGlobalScript = true;
    private boolean copyLocalScript = true;

    public ApplicationInterfaceController() {
        userHome = System.getProperty("user.home") + File.separator + "Siedelwood" + File.separator + "QSBSBuilder" + File.separator;
    }

    /**
     * Startet die grafische Oberfläche der Applikation.
     */
    public void start() {
        SwingUtilities.invokeLater(() -> {
            updateConfigurationQuietly();
            windowFrame = new MainWindow();
            windowFrame.setVersion(appVersion);
            windowFrame.setApplicationInterfaceController(ApplicationInterfaceController.this);
            windowFrame.setApplicationFeatureController(applicationFeatureController);
            windowFrame.initComponents();
            forceDownloadAllFiles();
        });
    }

    /**
     * Aktualisiert die Konfiguration schweigsam. Diese Methode arbeitet nur vor dem Start der GUI korrekt.
     */
    private void updateConfigurationQuietly() {
        try {
            LOG.info("Checking version of bundles configuration quietly...");
            if (isBundlesVersionOutdated()) {
                LOG.info("Updating now...");
                FileUtils.deleteQuietly(new File(userHome + pathToConfig + "/bundles.json"));
                Files.copy(Paths.get(userHome + pathToVar + File.separator + "release-bundles.json"), Paths.get(userHome + pathToConfig + File.separator + "bundles.json"));
                // TODO: Is not deleted somehow
                // FileUtils.deleteQuietly(new File(userHome + pathToVar + File.separator + "release-bundles.json"));
                LOG.info("Update complete!");
            }
            else {
                LOG.info("Configuration is up to date!");
            }
        }
        catch (Exception e) {
            LOG.error("Configuration can not be checked or updated!");
            e.printStackTrace();
        }
    }

    /**
     * Anzeige des nicht deterministischen Ladebalkens.
     */
    private void showOngoingProcessPanel() {
        hideAllPanel();
        windowFrame.setOngoingProcessPanelVisibility(true);
    }

    /**
     * Anzeige der Startseite.
     */
    private void showUserInterfacePanel() {
        hideAllPanel();
        windowFrame.setUserInterfacePanelVisibility(true);
    }

    /**
     * Anzeige des Update Interfaces.
     */
    private void showUpdateSourcesPanel(String text) {
        hideAllPanel();
        windowFrame.getUpdateSourcesPanel().getChangeLog().setText(text);
        windowFrame.setUpdateSourcesPanelVisibility(true);
    }

    /**
     * Anzeige des QSB Editor Panel.
     */
    private void showQsbEditorPanel() {
        hideAllPanel();
        windowFrame.setQsbEditorPanelVisibility(true);
    }

    private void showMapConversationPanel() {
        hideAllPanel();
        windowFrame.setMapConversationPanelVisibility(true);
    }

    /**
     * Alle Panel werden versteckt.
     */
    private void hideAllPanel() {
        windowFrame.setUserInterfacePanelVisibility(false);
        windowFrame.setOngoingProcessPanelVisibility(false);
        windowFrame.setUpdateSourcesPanelVisibility(false);
        windowFrame.setQsbEditorPanelVisibility(false);
        windowFrame.setMapConversationPanelVisibility(false);
    }

    /**
     * Initialer Download der Bibliothek.
     */
    private void forceDownloadAllFiles() {
        if (isAnySourceFileMissing()) {
            // Information anzeigen
            showOngoingProcessPanel();
            swingMessageService.displayInfoMessage(
                "Fehlende Quellen",
                "Das Programm hat festgestellt, dass mindestens eine Quelldatei fehlt.\n\nEs wird versucht ein" +
                " Update auszuführen, um diesen Missstand zu beseitigen!",
                windowFrame
            );
            // Initiales Update
            Thread t = new Thread(() -> {
                // Fehler
                if (!applicationFeatureController.forceUpdateFiles()) {
                    swingMessageService.displayErrorMessage(
                        "Fehler",
                        "Die Datei(en) konnte(n) nicht heruntergeladen werden!\n\nDas Programm kann nicht" +
                        " verwedet werden!",
                        windowFrame
                    );
                    hideAllPanel();
                    return;
                }
                // Erfolg
                swingMessageService.displayInfoMessage(
                    "Download abgeschlossen",
                    "Die benötigten Dateien wurden geladen!\n\nDas Programm kann jetzt verwendet werden.\n\n" +
                    "Denke daran, selbstständig regelmäßig auf Updates zu prüfen!",
                    windowFrame
                );
                showUserInterfacePanel();
            });
            t.start();
        }
    }

    /**
     * Führt die Suche nach Updates durch. Wenn ein Update gefunden wurde, kann der Anwender es anzeigen.
     */
    private void requestUpdate() {
        showOngoingProcessPanel();
        Thread t = new Thread(() -> {
            String update = applicationFeatureController.getPatchNotes();
            if ("".equals(update)) {
                String responseTitle;
                String responseText;
                Exception error = applicationFeatureController.getLastException();
                // Fehlernachricht anzeigen
                if (null != error) {
                    responseTitle = "Fehler";
                    responseText  = "Bei der Suche nach einem Update kam es zu einem unerwarteten Fehler!" +
                                    "\n\nDetails:\n" + ExceptionUtils.getStackTrace(error);
                    swingMessageService.displayErrorMessage(responseTitle, responseText, windowFrame);
                }
                // Kein Update Nachricht anzeigen
                else {
                    responseText = "Es konnte kein Update gefunden werden!";
                    responseTitle = "Kein Update";
                    swingMessageService.displayInfoMessage(responseTitle, responseText, windowFrame);
                }
                showUserInterfacePanel();
                return;
            }

            // Fragen, ob aktualisiert werden soll
            int result = swingMessageService.displayConfirmDialog(
                "Update gefunden",
                "Es gibt ein Update! Möchtest du es dir jetzt ansehen?",
                windowFrame
            );
            // Aktualisierung oder Abbruch
            if (result == 0) {
                showUpdateSourcesPanel(update);
            }
            else {
                showUserInterfacePanel();
            }
        });
        t.start();
    }

    /**
     * Führt das Update der Quelldateien aus. Mögliche aufgetretene Fehler werden angezeigt.
     */
    private void commenceUpdate() {
        showOngoingProcessPanel();
        Thread t = new Thread(() -> {
            applicationFeatureController.setLastException(null);
            // Fehler aufgetreten
            if (!applicationFeatureController.updateFiles()) {
                String errorText = "";
                Exception error = applicationFeatureController.getLastException();
                if (null != error) {
                    errorText = "\n\nDetails:\n" + ExceptionUtils.getStackTrace(error);
                }
                swingMessageService.displayErrorMessage(
                    "Fehler",
                    "Ein Fehler ist aufgetreten! Das Update konnte nicht heruntergeladen werden!" +errorText,
                    windowFrame
                );
                showUserInterfacePanel();
                return;
            }

            // Erfolgreiches Update
            swingMessageService.displayInfoMessage(
                "Update erfolgreich",
                "Alle Quelldateien der QSB wurden erfolgreich aktualisiert!",
                windowFrame
            );
            showUserInterfacePanel();
        });
        t.start();
    }

    /**
     * Baut die QSB zusammen und speichert sie im Zielverzeichnis. Basisskripte werden zusätzlich kopiert.
     */
    private void createQsb() {
        showOngoingProcessPanel();
        Thread t = new Thread(() -> {
            try {
                // Zielverzeichnis bestimmen
                if (null == lastDirectory) {
                    lastDirectory = new File(System.getProperty("user.home") + File.separator + "Downloads");
                }
                File destination = new File(lastDirectory.getAbsolutePath());
                // QSB bauen
                File qsb = applicationFeatureController.saveQsb(
                    destination.getAbsolutePath() + File.separator + "questsystembehavior.lua",
                    windowFrame.getQsbEditorPanel().getLoadOrderModel()
                );
                if (null == qsb) {
                    throw new Exception(applicationFeatureController.getLastException());
                }
                // Globales Skript kopieren
                if (copyGlobalScript) {
                    FileUtils.deleteQuietly(new File(destination.getAbsolutePath() + File.separator + "mapscript.lua"));
                    Files.copy(
                        Paths.get(userHome + pathToSources + File.separator + "mapscript.lua"),
                        Paths.get(destination.getAbsolutePath() + File.separator + "mapscript.lua")
                    );
                }
                // Lokales Skript kopieren
                if (copyLocalScript) {
                    FileUtils.deleteQuietly(new File(destination.getAbsolutePath() + File.separator + "localmapscript.lua"));
                    Files.copy(
                        Paths.get(userHome + pathToSources + File.separator + "localmapscript.lua"),
                        Paths.get(destination.getAbsolutePath() + File.separator + "localmapscript.lua")
                    );
                }
                // Erfolgsmeldung
                String destDirectoryText = "\n\nVerzeichnis:\n" +destination;
                swingMessageService.displayInfoMessage(
                    "Abgeschlossen",
                    "Die QSB wurde erstellt und im Zielverzeichnis gespeichert. " + destDirectoryText,
                    windowFrame
                );
                showUserInterfacePanel();
            }
            catch (Exception e) {
                // Fehlermeldung
                String errorText = "\n\nDetails:\n" +ExceptionUtils.getStackTrace(e);
                swingMessageService.displayErrorMessage(
                    "Fehler",
                    "Ein Fehler ist aufgetreten! Die QSB konnte nicht erstellt werden!" +errorText,
                    windowFrame
                );
                showUserInterfacePanel();
            }
        });
        t.start();
    }

    /**
     * Prüft ob eine der benötigten Quelldateien fehlt.
     * @return Datei fehlt
     */
    private boolean isAnySourceFileMissing() {
        try {
            LoadOrder loadOrder = loadOrderService.loadFromFileAndCreateLoadOrder(userHome + pathToConfig + File.separator + "bundles.json");

            if (!new File(userHome + pathToSources).exists()) {
                return true;
            }
            if (!new File(userHome + pathToSources + File.separator + "mapscript.lua").exists()) {
                return true;
            }
            if (!new File(userHome + pathToSources + File.separator + "localmapscript.lua").exists()) {
                return true;
            }

            for (QsbComponent c : loadOrder.getComponentList()) {
                String subfolder = "bundles" + File.separator;
                if (c.isAddon()) {
                    subfolder = "addons" + File.separator;
                }
                else if (c.isExternal()) {
                    subfolder = "externals" + File.separator;
                }
                else if ("Core".equals(c.getName())) {
                    subfolder = "";
                }
                if (!new File(userHome + pathToSources + File.separator + subfolder +c.getName().toLowerCase() + ".lua").exists()) {
                    return true;
                }
            }
        }
        catch (LoadOrderServiceException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Prüft ob sich die Konfiguration geändert hat.
     * @return Datei fehlt
     */
    private boolean isBundlesVersionOutdated() {
        try {
            LoadOrder localLoadOrder = loadOrderService.loadFromFileAndCreateLoadOrder(userHome + pathToConfig + File.separator + "bundles.json");
            if (!downloadManagerService.downloadFileFromGoogleDrive(driveIdBundles, userHome + pathToVar + File.separator + "release-bundles.json")) {
                throw new Exception();
            }
            LoadOrder remoteLoadOrder = loadOrderService.loadFromFileAndCreateLoadOrder(userHome + pathToVar + File.separator + "release-bundles.json");
            return localLoadOrder.compareTo(remoteLoadOrder) != 0;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Wählt eine Komponente aus oder entfernt sie.
     *
     * @param name      Name der Komponente
     * @param selected  Selection Flag
     * @param loadOrder Load Order
     */
    public void selectQsbComponent(String name, boolean selected, LoadOrder loadOrder) {
        loadOrder.setSelected(name, selected);
        windowFrame.getQsbEditorPanel().updateComponentCheckboxes();
    }

    /**
     * Setzt die Auswahl aller Komponenten zurück. Pflichtkomponenten bleiben ausgewählt.
     *
     * @param loadOrder Load Order
     */
    public void resetQsbComponents(LoadOrder loadOrder) {
        for (QsbComponent c : loadOrder.getComponentList()) {
            if (!c.isMandatory()) {
                loadOrder.getComponentSelectionMap().put(c.getName(), false);
            }
        }
        windowFrame.getQsbEditorPanel().updateComponentCheckboxes();
    }

    /**
     * Wählt alle Komponenten aus.
     *
     * @param loadOrder Load Order
     */
    public void selectAllQsbComponents(LoadOrder loadOrder) {
        for (QsbComponent c : loadOrder.getComponentList()) {
            loadOrder.getComponentSelectionMap().put(c.getName(), true);
        }
        windowFrame.getQsbEditorPanel().updateComponentCheckboxes();
    }

    /**
     * Öffnet einen File Chooser um das gewünschte Zielverzeichnis zu wählen.
     */
    public void openDestinationFileChooser() {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Projektverzeichnis wählen");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        if (null == lastDirectory) {
            lastDirectory = new File(System.getProperty("user.home") + File.separator + "Downloads");
        }
        chooser.setCurrentDirectory(lastDirectory);

        if(chooser.showOpenDialog(windowFrame) == JFileChooser.APPROVE_OPTION) {
            lastDirectory = chooser.getSelectedFile();
        }
    }

    // Listeners //

    @Override
    protected void onActionPerformed(ActionEvent event, JComponent source) {
        // Update //

        if ("SelectionPage_UpdateButton".equals(source.getName())) {
            requestUpdate();
        }
        if ("UpdatePage_CancleUpdate".equals(source.getName())) {
            showUserInterfacePanel();
        }
        if ("UpdatePage_ConfirmUpdate".equals(source.getName())) {
            commenceUpdate();
        }

        // Convert //

        if ("SelectionPage_ConvertButton".equals(source.getName())) {
            showMapConversationPanel();
        }
        if ("ConvertPage_DeclineConvert".equals(source.getName())) {
            showUserInterfacePanel();
        }
        if ("ConvertPage_ConfirmConvert".equals(source.getName())) {
            // TODO
        }

        // QSB Editor //

        if ("SelectionPage_ComposeButton".equals(source.getName())) {
            showQsbEditorPanel();
        }
        if ("EditorPage_DeclineSaving".equals(source.getName())) {
            showUserInterfacePanel();
        }
        if ("EditorPage_ConfirmSaving".equals(source.getName())) {
            createQsb();
        }
        if ("EditorPage_GlobalScript".equals(source.getName())) {
            copyGlobalScript = !copyGlobalScript;
        }
        if ("EditorPage_LocalScript".equals(source.getName())) {
            copyLocalScript = !copyLocalScript;
        }
        if ("EditorPage_OpenDocumentation".equals(source.getName())) {
            File htmlFile = new File(userHome + pathToSources + File.separator + "doc" + File.separator + "index.html");
            try {
                Desktop.getDesktop().browse(htmlFile.toURI());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onValueChanged(ListSelectionEvent event, JComponent source) {

    }
}

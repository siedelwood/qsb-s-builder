package twa.symfonia.qsbsbuilder.model.dependency;

import lombok.Getter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Model der Load Order
 *
 * Komponenten in der Load Order können selektiert und abgewählt werden.
 */
public class LoadOrder implements Comparable<LoadOrder> {
    @Getter
    private Map<String, Boolean> componentSelectionMap;
    @Getter
    private List<QsbComponent> componentList;
    @Getter
    private String componentsVersion;

    public LoadOrder() {
        componentSelectionMap = new LinkedHashMap<>();
        componentList = new ArrayList<>();
    }

    /**
     * Initialisiert die Load Oder mit der Liste der Komponenten.
     *
     * @param qsbComponentList Liste der Komponenten
     */
    public void init(List<QsbComponent> qsbComponentList, String version) {
        componentList = qsbComponentList;
        componentsVersion = version;
        componentSelectionMap = new LinkedHashMap<>();
        for (QsbComponent c : componentList) {
            componentSelectionMap.put(c.getName(), c.isMandatory());
        }
    }

    /**
     * Setzt den Selektiert-Status einer Komponente. Ist eine Komponente Pflicht (mandatory), dann kann sie nicht
     * abgewählt werden. Abhängigkeiten werden automatisch mit gesetzt.
     *
     * @param c Komponente aus der Load Order
     * @param selected Selektiert-Status
     */
    public void setSelected(QsbComponent c, boolean selected) {
        setSelected(c.getName(), selected);
    }

    /**
     * Setzt den Selektiert-Status einer Komponente. Ist eine Komponente Pflicht (mandatory), dann kann sie nicht
     * abgewählt werden. Abhängigkeiten werden automatisch mit gesetzt.
     *
     * @param name Name des Komponent
     * @param selected Selektiert-Status
     */
    public void setSelected(String name, boolean selected) {
        for (QsbComponent c : componentList) {
            if (name.equals(c.getName())) {
                if (c.isMandatory()) {
                    selected = true;
                }

                List<String> components;
                if (selected) {
                    components = getDependencies(c.getName());
                }
                else {
                    components = getAncestors(c.getName());
                }

                for (String component : components) {
                    setSelected(component, selected);
                }
                break;
            }
        }
        componentSelectionMap.put(name, selected);
    }

    /**
     * Gibt alle selektierten Komponenten in der richtigen Reihenfolge zurück.
     *
     * @return Liste der gewählten Komponenten
     */
    public List<String> getSelectedComponents() {
        List<String> list = new ArrayList<>();
        for (Map.Entry<String, Boolean> e : componentSelectionMap.entrySet()) {
            if (e.getValue()) {
                list.add(e.getKey());
            }
        }
        return list;
    }

    /**
     * Gibt die Namen aller abhängigen Komponenten der Komponente zurück.
     *
     * @param name Name der Komponente
     * @return Liste mit Komponentennamen
     */
    public List<String> getDependencies(String name) {
        List<String> list = new ArrayList<>();
        QsbComponent c = getComponentByName(name);
        for (int i=0; i<c.getDependencies().size(); i++) {
            String dependencyName = c.getDependencies().get(i);
            // Dependencies der Dependency rekursiv laden
            List<String> dependencDependencies = getDependencies(dependencyName);
            for (String s : dependencDependencies) {
                if (!list.contains(s)) {
                    list.add(s);
                }
            }
            // Dependency hinzufügen
            if (!list.contains(dependencyName)) {
                list.add(dependencyName);
            }
        }
        return list;
    }

    /**
     * Gibt die Namen aller Komponenten zurük, die von der Komponente abhängig sind.
     *
     * @param name Name der Komponente
     * @return Liste mit Komponentennamen
     */
    public List<String> getAncestors(String name) {
        List<String> list = new ArrayList<>();
        QsbComponent c = getComponentByName(name);
        for (QsbComponent qsbComponent : componentList) {
            String dependencyName = qsbComponent.getName();
            if (qsbComponent.getDependencies().contains(c.getName())) {
                // Ancestors des Ancestor rekursiv laden
                List<String> innerList = getAncestors(dependencyName);
                for (String s : innerList) {
                    if (!list.contains(s)) {
                        list.add(s);
                    }
                }
                // Ancestor hinzufügen
                if (!list.contains(dependencyName)) {
                    list.add(dependencyName);
                }
            }
        }
        return list;
    }

    /**
     * Gibt das Model der Komponente mit dem Namen zurück.
     *
     * @param name Name der Komponente
     * @return Modell Object
     */
    public QsbComponent getComponentByName(String name) {
        for (QsbComponent c : componentList) {
            if (name.equals(c.getName())) {
                return c;
            }
        }
        return null;
    }

    /**
     * Vergleicht die Version mit einer anderen Version.
     *
     * @param o Andere Version
     * @return Vergleichswert
     */
    @Override
    public int compareTo(LoadOrder o) {
        String[] localVersion = componentsVersion.split("\\.");
        String[] otherVersion = o.componentsVersion.split("\\.");

        int difference = 0;
        difference += localVersion[0].compareTo(otherVersion[0]);
        if (difference != 0) {
            return difference;
        }
        difference += localVersion[1].compareTo(otherVersion[1]);
        if (difference != 0) {
            return difference;
        }
        difference += localVersion[2].compareTo(otherVersion[2]);
        return difference;
    }
}

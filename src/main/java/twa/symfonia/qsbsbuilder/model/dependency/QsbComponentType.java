package twa.symfonia.qsbsbuilder.model.dependency;

/**
 * Typen von QSB-Komponenten
 */
public enum QsbComponentType {
    /**
     * Unabhänrige Komponente, welche nur von Core abhängig ist.
     */
    BUNDLE,

    /**
     * Externe Komponente, dass sowohl von Bundles als auch AddOns abhängen kann.
     */
    EXTERNAL,

    /**
     * Komponente, die von mindestens einer weiteren neben Core abhängt.
     */
    ADDON
}

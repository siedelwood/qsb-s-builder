package twa.symfonia.qsbsbuilder.model.dependency;

import com.github.cliftonlabs.json_simple.JsonObject;
import com.github.cliftonlabs.json_simple.Jsonable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

/**
 * Model zur internen Repräsentation eines Eintrages in der Load Order der QSB-Komponenten.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QsbComponent implements Jsonable, Comparable {
    private QsbComponentType type = QsbComponentType.BUNDLE;
    private String name;
    private String displayName;
    private String description;
    private List<String> dependencies;
    private boolean mandatory;
    private boolean addon = false;
    private boolean external = false;

    /**
     * {@inheritDoc}
     * @return Json String
     */
    @Override
    public String toJson() {
        final StringWriter writable = new StringWriter();
        try {
            this.toJson(writable);
        }
        catch (final IOException e) {}
        return writable.toString();
    }

    /**
     * {@inheritDoc}
     * @param writer Writer
     * @throws IOException
     */
    @Override
    public void toJson(Writer writer) throws IOException {
        final JsonObject json = new JsonObject();
        json.put("name", name);
        json.put("displayName", displayName);
        json.put("description", description);
        json.put("dependencies", dependencies);
        json.put("mandatory", mandatory);
        json.put("addon", addon);
        json.put("external", external);
        json.toJson(writer);
    }

    /**
     * Vergleicht zwei Objekte miteinander.
     *
     * Objekte werden alphabetisch nach Name verglichen. Wenn sie pflicht (mandatory) sind, haben sie vorrang for
     * einem Objekt, das nicht pflicht ist.
     *
     * @param o Other
     * @return Vergleichswert
     */
    @Override
    public int compareTo(Object o) {
        int compared = displayName.compareTo(((QsbComponent) o).displayName);
        if (compared == 0) {
            if (((QsbComponent) o).mandatory) {
                return 1;
            }
        }
        return 0;
    }
}

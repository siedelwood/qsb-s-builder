package twa.symfonia.qsbsbuilder.model.dependency;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QsbComponentContainer {
    private String version;
    private List<QsbComponent> components;
}

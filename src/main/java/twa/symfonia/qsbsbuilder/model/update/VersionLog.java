package twa.symfonia.qsbsbuilder.model.update;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Modell eines Eintrages im Change Log.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VersionLog {
    private String version;
    private List<String> changes;
}

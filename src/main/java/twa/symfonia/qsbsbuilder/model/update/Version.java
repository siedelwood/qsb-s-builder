package twa.symfonia.qsbsbuilder.model.update;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Modell für die interne Darstellung der Version.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Version implements Comparable<Version> {
    private String version;
    private String date;
    private List<VersionLog> changelog;

    /**
     * Vergleicht die Version mit einer anderen Version
     * @param o Andere Version
     * @return Vergleichswert
     */
    @Override
    public int compareTo(Version o) {
        String[] localVersion = version.split("\\.");
        String[] otherVersion = o.version.split("\\.");

        if (localVersion.length != 3 || otherVersion.length != 3) {
            return 0;
        }
        int major = localVersion[0].compareTo(otherVersion[0]);
        if (major != 0) {
            return major;
        }
        int minor = localVersion[1].compareTo(otherVersion[1]);
        if (minor != 0) {
            return minor;
        }
        int bugfix = localVersion[2].compareTo(otherVersion[2]);
        if (bugfix != 0) {
            return bugfix;
        }
        return 0;
    }

    /**
     * Gibt alle Patch Nodes zurück, die ab der angegebenen Version hinzukamen
     * @param startVersion  Version
     * @return Änderungen
     */
    public List<VersionLog> getPatchNotes(String startVersion) {
        List<VersionLog> patchNotes = new ArrayList<>();
        for (VersionLog e : changelog) {
            String[] localVersion = e.getVersion().split("\\.");
            String[] otherVersion = startVersion.split("\\.");
            if (localVersion[0].compareTo(otherVersion[0]) < 0 || localVersion[1].compareTo(otherVersion[1]) < 0 || localVersion[2].compareTo(otherVersion[2]) < 0) {
                patchNotes.add(e);
            }
        }
        return patchNotes;
    }
}

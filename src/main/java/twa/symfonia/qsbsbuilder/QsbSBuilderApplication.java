package twa.symfonia.qsbsbuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Application
 */
@SpringBootApplication
public class QsbSBuilderApplication {
    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(QsbSBuilderApplication.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);
        // SpringApplication.run(QsbSBuilderApplication.class, args);
    }
}
